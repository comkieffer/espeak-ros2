# Copyright 2021 Thibaud Chupin
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import sys
import time
from typing import List, Optional
from subprocess import run


import rclpy
from rclpy.node import Node
from espeak_interfaces.msg import Sentence


def locate_espeak_binary() -> Optional[str]:
    status = run("hash espeak-ng", shell=True)
    if status.returncode == 0:
        return "espeak-ng"

    status = run("hash espeak", shell=True)
    if status.returncode == 0:
        return "espeak"

    return None


class EspeakNode(Node):
    def __init__(self):
        super().__init__("text_to_speech")

        self.espeak_binary = locate_espeak_binary()
        if not self.espeak_binary:
            raise RuntimeError(
                "Cannot initialise 'espeak_ros2' node. Could not find binaries for "
                "`espeak` or `espeak-ng`."
            )
        self.get_logger().info(f"Using {self.espeak_binary} for speech synthesis.")

        self.message_text_sub = self.create_subscription(
            Sentence, "text_to_speech", self._on_tts_message, 10
        )

        self.get_logger().info(
            f"{self.get_name()} initialised. Waiting for messages ..."
        )

    def say(
        self,
        text: str,
        amplitude: int = None,
        word_gap: int = None,
        pitch: int = None,
        words_per_minute: int = None,
        voice_name: str = None,
        extra_args: List[str] = [],
    ) -> None:
        espeak_args = extra_args
        if amplitude and amplitude != -1:
            espeak_args.extend(["-a", str(amplitude)])
        if word_gap and word_gap != -1:
            espeak_args.extend(["-g", str(word_gap)])
        if pitch and pitch != -1:
            espeak_args.extend(["-p", str(pitch)])
        if words_per_minute and words_per_minute != -1:
            espeak_args.extend(["-s", str(words_per_minute)])
        if voice_name and voice_name != "":
            espeak_args.extend(["-v", voice_name])

        # If the command return non-zero, a CalledProcessError is raised
        espeak_cmd = [self.espeak_binary, text, *espeak_args]
        self.get_logger().debug(f"{espeak_cmd=}")

        ret = run(espeak_cmd, capture_output=True, universal_newlines=True)
        if ret.returncode != 0 or len(ret.stderr) != 0:
            self.get_logger().error(
                f"Call to `{espeak_cmd}` failed (code = {ret.returncode})\n"
                f"## Captured stderr = \n{ret.stderr}"
            )

    def _on_tts_message(self, msg: Sentence) -> None:
        self.get_logger().info(f"Saying '{msg.text}'")
        time_start = time.time()
        self.say(
            text=msg.text,
            amplitude=msg.settings.amplitude,
            word_gap=msg.settings.word_gap,
            pitch=msg.settings.pitch,
            words_per_minute=msg.settings.words_per_minute,
            voice_name=msg.settings.voice_name,
        )
        self.get_logger().debug(
            f"Espeak completed phrase in {time.time() - time_start:.1f} s."
        )


def main(args=None):
    rclpy.init(args=args)

    minimal_subscriber = EspeakNode()
    rclpy.spin(minimal_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    minimal_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass

    rclpy.loginfo("Exiting gracefully ...")
    sys.exit(0)
