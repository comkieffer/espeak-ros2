from setuptools import setup, find_packages

package_name = "espeak_ros"

setup(
    name=package_name,
    version="0.1.0",
    packages=find_packages(exclude=["test"]),
    data_files=[
        ("share/ament_index/resource_index/packages", ["resource/" + package_name]),
        ("share/" + package_name, ["package.xml"]),
    ],
    install_requires=["setuptools"],
    zip_safe=True,
    author="Thibaud Chupin",
    author_email="thibaud.chupin@gmail.com",
    maintainer="Thibaud Chupin",
    maintainer_email="thibaud.chupin@gmail.com",
    url="https://gitlab.com/comkieffer/espeak-ros2/",
    project_urls={"Bug Tracker": "https://gitlab.com/comkieffer/espeak-ros2/issues/"},
    description="ROS interface to the `espeak` command line tool.",
    license="MIT",
    tests_require=["pytest"],
    entry_points={
        "console_scripts": [
            "espeak = espeak_ros.__main__:main",
        ],
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
    ],
)
