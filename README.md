# Espeak -  ROS2

ROS 2 interface to the `espeak-ng` command line tool. The repository is composed of the `espeak` package which implements the interface to the command line tool and the `espeak_interfaces` package which contains the message definitions.

## Installation

The package depends on the `espeak-ng` executable. On Ubuntu, it can be downloaded with

``` console
$ apt install espeak-ng
```

## Usage

The `espeak` node acts as a server. It responds to `espeak_itnerfaces/Sentence` commands sent to it by other nodes. Start the server with

``` console
$ ros2 run espeak_ros espeak
```

and send it commands (using the command line) with

``` console
$ ros2 topic pub --once text_to_speech espeak_interfaces/Sentence \
    "text: Welcome to the espeak ROS node!"
```

The speed, pitch, and other `espeak-ng` parameters can be configured in the message itself:

``` console
$ ros2 topic pub --once text_to_speech espeak_interfaces/Sentence \
    '{text: "Welcome to the espeak ROS node!", settings: {words_per_minute: 100}}'
```

For a full list of supported options, read [VoiceSettings.msg](espeak_interfaces/msg/VoiceSettings.msg).

## Usage Inside a Docker Container


To output sound from the container, PulseAudio needs to be configured to expose a unix socket. Install `paprefs`

``` console
apt install paprefs
```

On Ubuntu 20.04, a symlink needs to be created to enable `paprefs` to detect the PulseAudio configuration:

``` console
ln -s /usr/lib/pulse-13.99.1/ /usr/lib/pulse-13.99
```

Run `paprefs`, and tick `Network Server > Enable network access to local sound devices`. After closing, restart the computer (restarting pulseaudio seems to not be enough).

Then, add the following to your docker run command

``` console
$ docker run ... \
    --env "PULSE_SERVER=unix:/run/user/$(id -u)/pulse/native"                  \
    --env "PULSE_COOKIE=/run/pulse/cookie"                                     \
    --volume ~/.config/pulse/cookie:/run/pulse/cookie                          \
    --volume /run/user/$(id -u)/pulse/native:/run/user/$(id -u)/pulse/native
)
```

This maps the pulseaudio unix domain socket between the host and the container and passes the authentication cookie to the container.

## Development

Ensure that the style tests pass before pusshing code. To fix copyright issues run:

``` console
$ ament_copyright --add-missing "Thibaud Chupin" mit
```

To release a new version of the package:

- update the `CHANGELOG.rst`

  ``` console
  $ catkin_prepare_release
  ```

  For _non-patch_ releases, pass `--bump major` or `--bump minor`.

- Create the new relase with `bloom-release` and let it create the PR on `rosdistro`

  ``` console
  $ bloom-release --rosdistro foxy espeak_ros
  ```
